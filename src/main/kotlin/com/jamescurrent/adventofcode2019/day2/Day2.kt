package com.jamescurrent.adventofcode2019.day2

fun List<Int>.executeProgram(noun: Int = this[1], verb: Int = this[2]): Int {
    val workingMemory = this.toMutableList().apply {
        this[1] = noun
        this[2] = verb
    }

    for (commandIndex in workingMemory.indices step 4) {
        val command = workingMemory[commandIndex]

        check(listOf(1, 2, 99).contains(command)) {
            "invalid command: $command at position $commandIndex"
        }

        when (command) {
            1 -> workingMemory.runAdd(commandIndex)
            2 -> workingMemory.runMulti(commandIndex)
            99 -> return workingMemory.first()
        }
    }
    throw IllegalStateException("No more commands :(")
}

fun List<Int>.runPart2(): Int {
    for (noun in 0..99) {
        for (verb in 0..99) {
            if (this.executeProgram(noun, verb) == 19690720) {
                return noun * 100 + verb
            }
        }
    }
    throw IllegalStateException("No idea what the answer is... ¯\\_(ツ)_/¯")
}

private fun MutableList<Int>.runAdd(startIndex: Int) {
    this[this[startIndex + 3]] = this[this[startIndex + 1]] + this[this[startIndex + 2]]
}

private fun MutableList<Int>.runMulti(startIndex: Int) {
    this[this[startIndex + 3]] = this[this[startIndex + 1]] * this[this[startIndex + 2]]
}

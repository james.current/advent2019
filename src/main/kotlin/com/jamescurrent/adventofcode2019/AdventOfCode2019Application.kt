package com.jamescurrent.adventofcode2019

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class AdventOfCode2019Application

fun main(args: Array<String>) {
    runApplication<AdventOfCode2019Application>(*args)
}

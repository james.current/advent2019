package com.jamescurrent.adventofcode2019.day6

fun String.orbitSplit(): Pair<String, String> =
    split(")").run {
        first() to last()
    }

data class Node(
    val id: String,
    val orbits: List<Node>
)

fun List<Pair<String, String>>.makeNode(id: String = "COM"): Node {
    val orbitIds = this
        .filter { it.first == id }
        .map { it.second }

    val orbits = orbitIds.map { makeNode(it) }

    return Node(id, orbits)
}

fun Node.walkOrbits(depth: Int = 0): Int =
    orbits.fold(depth) { orbitCount, node ->
        orbitCount + node.walkOrbits(depth + 1)
    }

fun List<Pair<String, String>>.makeOrbitMap(): Map<String, List<String>> =
    map { (orbitId, _) ->
        orbitId to filter { it.first == orbitId }.map { it.second }
    }.toMap()

fun Map<String, List<String>>.findOrbits(satelliteId: String, baseOrbitId: String = "COM"): List<String> {
    val orbitId = filterValues { it.contains(satelliteId) }.keys.single()

    return if (orbitId == baseOrbitId) {
        listOf(orbitId)
    } else {
        listOf(orbitId).plus(findOrbits(orbitId, baseOrbitId))
    }
}

fun List<String>.findCommonOrbit(otherOrbits: List<String>): String =
    reduceIndexed { index, lastCommonOrbit, orbit ->
        if (orbit != otherOrbits[index]) {
            return lastCommonOrbit
        }

        orbit
    }

fun List<String>.findMinOrbitalTransfersToSanta(): Int {

    val orbitMap = map { it.orbitSplit() }.makeOrbitMap()

    val youOrbitList = orbitMap.findOrbits("YOU").reversed()
    val santaOrbitList = orbitMap.findOrbits("SAN").reversed()

    val commonOrbit = youOrbitList.findCommonOrbit(santaOrbitList)

    val youHopsToCommonOrbit = orbitMap.findOrbits("YOU", commonOrbit).size - 1
    val santaHopsToCommonOrbit = orbitMap.findOrbits("SAN", commonOrbit).size - 1

    return youHopsToCommonOrbit + santaHopsToCommonOrbit
}

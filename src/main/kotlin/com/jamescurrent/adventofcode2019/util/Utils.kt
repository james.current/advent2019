package com.jamescurrent.adventofcode2019.util

inline fun <reified R> MutableList<R>.pop(): R = this.removeAt(0)

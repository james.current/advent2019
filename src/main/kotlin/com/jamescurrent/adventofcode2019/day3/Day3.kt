package com.jamescurrent.adventofcode2019.day3

import kotlin.math.abs

fun String.getMovement(): Pair<Int, Int> =
    when (this.first()) {
        'U' -> 0 to this.substring(1).toInt()
        'D' -> 0 to this.substring(1).toInt() * -1
        'R' -> this.substring(1).toInt() to 0
        'L' -> this.substring(1).toInt() * -1 to 0
        else -> throw IllegalArgumentException("Not sure what to do with ${this.first()}")
    }

operator fun Pair<Int, Int>.plus(move: Pair<Int, Int>) =
    this.first + move.first to this.second + move.second

fun Pair<Int, Int>.fillLine(movement: Pair<Int, Int>): List<Pair<Int, Int>> {
    fun fillX(pointRange: IntRange): List<Pair<Int, Int>> =
        pointRange.fold(listOf()) { points, x ->
            points.plus(x to this.second)
        }

    fun fillY(pointRange: IntRange): List<Pair<Int, Int>> =
        pointRange.fold(listOf()) { points, y ->
            points.plus(this.first to y)
        }

    val end = this + movement
    return when {
        movement.first > 0 ->
            fillX((this.first + 1)..end.first)
        movement.first < 0 ->
            fillX(end.first until this.first).reversed()
        movement.second > 0 ->
            fillY((this.second + 1)..end.second)
        movement.second < 0 ->
            fillY(end.second until this.second).reversed()
        else -> listOf()
    }
}

fun List<Pair<Int, Int>>.convertMovementToCoordinateList() =
    this.fold(listOf(0 to 0)) { coordinateList, movement ->
        coordinateList.plus(coordinateList.last().fillLine(movement))
    }

fun Pair<Int, Int>.calculateManhattanDistance(): Int =
    abs(this.first) + abs(this.second)

fun part1(wire1Moves: List<String>, wire2Moves: List<String>): Int {
    val wire1 = wire1Moves
        .map(String::getMovement)
        .convertMovementToCoordinateList()
        .toSet()
    val wire2 = wire2Moves
        .map(String::getMovement)
        .convertMovementToCoordinateList()
        .toSet()

    val intersectionDistances = wire1
        .intersect(wire2)
        .map(Pair<Int, Int>::calculateManhattanDistance)

    return intersectionDistances.minus(0).min()!!
}

fun part2(wire1Moves: List<String>, wire2Moves: List<String>): Int {
    val wire1 = wire1Moves
        .map(String::getMovement)
        .convertMovementToCoordinateList()
    val wire2 = wire2Moves
        .map(String::getMovement)
        .convertMovementToCoordinateList()

    val intersectionsSteps = wire1
        .toSet()
        .intersect(wire2.toSet())
        .map { it.getSteps(wire1, wire2) }

    return intersectionsSteps.minus(0).min()!!
}

fun Pair<Int, Int>.getSteps(wire1: List<Pair<Int, Int>>, wire2: List<Pair<Int, Int>>): Int =
    wire1.indexOf(this) + wire2.indexOf(this)

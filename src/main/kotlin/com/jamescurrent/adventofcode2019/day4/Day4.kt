package com.jamescurrent.adventofcode2019.day4

fun Int.isPossiblePasswordPartOne(): Boolean =
    with(toDigitList()) {
        hasAdjacent() && isIncreasing()
    }

fun Int.isPossiblePasswordPartTwo(): Boolean =
    with(toDigitList()) {
        hasPair() && isIncreasing()
    }

fun List<Int>.hasAdjacent(): Boolean {
    for (index in 1 until size) {
        if (this[index] == this[index - 1]) {
            return true
        }
    }
    return false
}

fun List<Int>.hasPair(): Boolean =
    groupingBy { it }
        .eachCount()
        .any { it.value == 2 }

fun List<Int>.isIncreasing(): Boolean {
    for (index in 1 until size) {
        if (this[index] < this[index - 1]) {
            return false
        }
    }
    return true
}

fun Int.toDigitList(): List<Int> = toString().map(Character::getNumericValue)

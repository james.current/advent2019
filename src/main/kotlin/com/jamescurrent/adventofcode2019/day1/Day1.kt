package com.jamescurrent.adventofcode2019.day1

import kotlin.math.floor

fun Double.calculateFuel(): Double = floor(this / 3.0) - 2

fun Double.calculateActualFuel(): Double =
    generateSequence(this.calculateFuel()) { fuel ->
        fuel.calculateFuel().takeIf { it > 0.0 }
    }.sum()

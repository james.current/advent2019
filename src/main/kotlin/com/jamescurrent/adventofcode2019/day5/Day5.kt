package com.jamescurrent.adventofcode2019.day5

import com.jamescurrent.adventofcode2019.util.pop

fun List<Int>.executeProgram(input: List<Int>): List<Int> {
    val workingMemory = toMutableList()
    val inputStack = input.toMutableList()
    val outputList = mutableListOf<Int>()

    workingMemory.foldIndexed(0) { index, instructionPointer, instruction ->
        if (index != instructionPointer) {
            return@foldIndexed instructionPointer
        }

        val opcode = instruction.getOpcode()
        val modes = instruction / 100
        instructionPointer + when (opcode) {
            1, 2 -> workingMemory.runAddOrMulti(instructionPointer, modes)
            3 -> workingMemory.consumeInput(instructionPointer, inputStack)
            4 -> workingMemory.output(instructionPointer, outputList)
            5, 6 -> return@foldIndexed workingMemory.jumpIf(instructionPointer, modes) ?: instructionPointer + 3
            7, 8 -> workingMemory.runLessThanOrEquals(instructionPointer, modes)
            99 -> return outputList
            else -> throw IllegalArgumentException("invalid opcode: $opcode at position $instructionPointer")
        }
    }

    throw IllegalStateException("Ran out off the end of the program :(")
}

private fun Int.getOpcode(): Int = this % 100

private fun Int.getMode(): Int = this % 10

private fun MutableList<Int>.runAddOrMulti(startIndex: Int, modes: Int): Int {
    val firstParamMode = modes.getMode()
    val secondParamMode = (modes / 10).getMode()

    val firstParamValue = getParam(startIndex + 1, firstParamMode)
    val secondParamValue = getParam(startIndex + 2, secondParamMode)

    if (this[startIndex].getOpcode() == 1) {
        this[this[startIndex + 3]] = firstParamValue + secondParamValue
    } else {
        this[this[startIndex + 3]] = firstParamValue * secondParamValue
    }

    return 4
}

private fun MutableList<Int>.getParam(index: Int, mode: Int) =
    when (mode) {
        0 -> this[this[index]]
        1 -> this[index]
        else -> throw IllegalArgumentException("Illegal mode $mode for index $index")
    }

private fun MutableList<Int>.consumeInput(startIndex: Int, inputStack: MutableList<Int>): Int {
    val param = getParam(startIndex + 1, 1)

    this[param] = inputStack.pop()

    return 2
}

private fun MutableList<Int>.output(startIndex: Int, outputList: MutableList<Int>): Int {
    val param = getParam(startIndex + 1, 1)

    outputList.add(this[param])

    return 2
}

private fun MutableList<Int>.jumpIf(startIndex: Int, modes: Int): Int? {
    val firstParamMode = modes.getMode()
    val secondParamMode = (modes / 10).getMode()

    val firstParamValue = getParam(startIndex + 1, firstParamMode)
    val secondParamValue = getParam(startIndex + 2, secondParamMode)

    val isZero = firstParamValue == 0

    return if (this[startIndex].getOpcode() == 5) {
        secondParamValue.takeIf { !isZero }
    } else {
        secondParamValue.takeIf { isZero }
    }
}

private fun MutableList<Int>.runLessThanOrEquals(startIndex: Int, modes: Int): Int {
    val firstParamMode = modes.getMode()
    val secondParamMode = (modes / 10).getMode()

    val firstParamValue = getParam(startIndex + 1, firstParamMode)
    val secondParamValue = getParam(startIndex + 2, secondParamMode)

    if (this[startIndex].getOpcode() == 7) {
        runPredicate(this[startIndex + 3]) { firstParamValue < secondParamValue }
    } else {
        runPredicate(this[startIndex + 3]) { firstParamValue == secondParamValue }
    }

    return 4
}

private inline fun MutableList<Int>.runPredicate(storageIndex: Int, predicate: () -> Boolean) =
    this.set(
        storageIndex,
        if (predicate()) {
            1
        } else {
            0
        }
    )

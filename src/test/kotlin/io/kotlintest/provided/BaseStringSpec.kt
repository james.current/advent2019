package io.kotlintest.provided

import io.kotlintest.IsolationMode
import io.kotlintest.specs.StringSpec

open class BaseStringSpec : StringSpec() {
    override fun isolationMode(): IsolationMode = IsolationMode.InstancePerTest
}
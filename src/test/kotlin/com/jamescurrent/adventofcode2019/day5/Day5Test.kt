package com.jamescurrent.adventofcode2019.day5

import com.jamescurrent.adventofcode2019.util.getInputText
import com.jamescurrent.adventofcode2019.util.output
import io.kotlintest.provided.BaseStringSpec
import io.kotlintest.shouldBe

internal class Day5Test : BaseStringSpec() {
    init {
        val program = getInputText("day5.txt").split(",").map(String::toInt)

        "Part 1 answer" {
            val answer = program.executeProgram(listOf(1)).last()

            output(
                day = 5,
                part = 1,
                answer = answer
            )

            answer shouldBe 15386262
        }

        "Part 2 answer" {
            val answer = program.executeProgram(listOf(5)).last()

            output(
                day = 5,
                part = 2,
                answer = answer
            )

            answer shouldBe 10376124
        }
    }
}

package com.jamescurrent.adventofcode2019.day2

import com.jamescurrent.adventofcode2019.util.getInputText
import com.jamescurrent.adventofcode2019.util.output
import io.kotlintest.data.forall
import io.kotlintest.provided.BaseStringSpec
import io.kotlintest.shouldBe
import io.kotlintest.tables.row

internal class Day2Test : BaseStringSpec() {
    init {
        val program = getInputText("day2.txt").split(",").map(String::toInt)

        "Part 1 given examples" {
            forall(
                row(listOf(1, 0, 0, 0, 99), listOf(2, 0, 0, 0, 99)),
                row(listOf(2, 3, 0, 3, 99), listOf(2, 3, 0, 6, 99)),
                row(listOf(2, 4, 4, 5, 99, 0), listOf(2, 4, 4, 5, 99, 9801)),
                row(listOf(1, 1, 1, 4, 99, 5, 6, 0, 99), listOf(30, 1, 1, 4, 2, 5, 6, 0, 99))
            ) { input, output ->
                input.executeProgram() shouldBe output[0]
            }
        }

        "Part 1 answer" {
            val finalState = program.executeProgram(
                noun = 12,
                verb = 2
            )

            output(
                day = 2,
                part = 1,
                answer = finalState
            )

            finalState shouldBe 9581917
        }

        "Part 2 answer" {
            val finalState = program.runPart2()

            output(
                day = 2,
                part = 2,
                answer = finalState
            )
        }
    }
}

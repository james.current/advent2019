package com.jamescurrent.adventofcode2019.util

import io.kotlintest.provided.BaseStringSpec
import java.net.URI
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.streams.asSequence

fun URI.readLines(): Sequence<String> = Files.lines(Paths.get(this)).asSequence()

fun BaseStringSpec.getInputLines(fileName: String): Sequence<String> =
    javaClass
        .getResource("/$fileName")
        .toURI()
        .readLines()

fun BaseStringSpec.getInputText(fileName: String): String =
    javaClass
        .getResource("/$fileName")
        .readText()
        .trim()

fun output(day: Int, part: Int, answer: Any) =
    println(
        """
        Day $day Part $part
        $answer
        """.trimIndent()
    )

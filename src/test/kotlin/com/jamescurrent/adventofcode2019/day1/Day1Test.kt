package com.jamescurrent.adventofcode2019.day1

import com.jamescurrent.adventofcode2019.util.getInputLines
import com.jamescurrent.adventofcode2019.util.output
import io.kotlintest.data.forall
import io.kotlintest.provided.BaseStringSpec
import io.kotlintest.shouldBe
import io.kotlintest.tables.row

internal class Day1Test : BaseStringSpec() {
    init {
        "Part 1 given examples" {
            forall(
                row(12.0, 2.0),
                row(14.0, 2.0),
                row(1969.0, 654.0),
                row(100756.0, 33583.0)
            ) { mass, fuel ->
                mass.calculateFuel() shouldBe fuel
            }
        }

        "Part 1 answer" {
            val totalFuel = getInputLines("day1.txt")
                .map(String::toDouble)
                .map(Double::calculateFuel)
                .sum()

            output(
                day = 1,
                part = 1,
                answer = totalFuel
            )
        }

        "Part 2 given examples" {
            forall(
                row(14.0, 2.0),
                row(1969.0, 966.0),
                row(100756.0, 50346.0)
            ) { mass, fuel ->
                mass.calculateActualFuel() shouldBe fuel
            }
        }

        "Part 2 answer" {
            val totalFuel = getInputLines("day1.txt")
                .map(String::toDouble)
                .map(Double::calculateActualFuel)
                .sum()

            output(
                day = 1,
                part = 2,
                answer = totalFuel
            )
        }
    }
}
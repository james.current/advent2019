package com.jamescurrent.adventofcode2019.day4

import com.jamescurrent.adventofcode2019.util.output
import io.kotlintest.data.forall
import io.kotlintest.provided.BaseStringSpec
import io.kotlintest.shouldBe
import io.kotlintest.tables.row

internal class Day4Test : BaseStringSpec() {
    val input = 382345..843167

    init {
        "Part 1 given examples" {
            forall(
                row(111111, true),
                row(223450, false),
                row(123789, false)
            ) { password, passes ->
                password.isPossiblePasswordPartOne() shouldBe passes
            }
        }

        "Part 1 answer" {
            val possiblePasswordCount = input
                .filter { it.isPossiblePasswordPartOne() }
                .count()

            output(
                day = 4,
                part = 1,
                answer = possiblePasswordCount
            )

            possiblePasswordCount shouldBe 460
        }

        "Part 2 given examples" {
            forall(
                row(112233, true),
                row(123444, false),
                row(111122, true)
            ) { password, passes ->
                password.isPossiblePasswordPartTwo() shouldBe passes
            }
        }

        "Part 2 answer" {
            val possiblePasswordCount = input
                .filter { it.isPossiblePasswordPartTwo() }
                .count()

            output(
                day = 4,
                part = 2,
                answer = possiblePasswordCount
            )

            possiblePasswordCount shouldBe 290
        }
    }
}
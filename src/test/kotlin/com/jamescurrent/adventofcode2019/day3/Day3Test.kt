package com.jamescurrent.adventofcode2019.day3

import com.jamescurrent.adventofcode2019.util.getInputLines
import com.jamescurrent.adventofcode2019.util.output
import io.kotlintest.data.forall
import io.kotlintest.provided.BaseStringSpec
import io.kotlintest.shouldBe
import io.kotlintest.tables.row

internal class Day3Test : BaseStringSpec() {
    init {
        "Part 1 given examples" {
            forall(
                row(
                    listOf("R8", "U5", "L5", "D3"),
                    listOf("U7", "R6", "D4", "L4"),
                    6
                ),
                row(
                    listOf("R75", "D30", "R83", "U83", "L12", "D49", "R71", "U7", "L72"),
                    listOf("U62", "R66", "U55", "R34", "D71", "R55", "D58", "R83"),
                    159
                ),
                row(
                    listOf("R98", "U47", "R26", "D63", "R33", "U87", "L62", "D20", "R33", "U53", "R51"),
                    listOf("U98", "R91", "D20", "R16", "D67", "R40", "U7", "R15", "U6", "R7"),
                    135
                )
            ) { wire1, wire2, expectedAnswer ->
                part1(wire1, wire2) shouldBe expectedAnswer
            }
        }

        "Part 1 answer" {
            val (wire1, wire2) =
                getInputLines("day3.txt")
                    .map { it.split(",") }
                    .toList()

            val distance = part1(wire1, wire2)

            output(
                day = 3,
                part = 1,
                answer = distance
            )

            distance shouldBe 2129
        }

        "Part 2 given examples" {
            forall(
                row(
                    listOf("R8", "U5", "L5", "D3"),
                    listOf("U7", "R6", "D4", "L4"),
                    30
                ),
                row(
                    listOf("R75", "D30", "R83", "U83", "L12", "D49", "R71", "U7", "L72"),
                    listOf("U62", "R66", "U55", "R34", "D71", "R55", "D58", "R83"),
                    610
                ),
                row(
                    listOf("R98", "U47", "R26", "D63", "R33", "U87", "L62", "D20", "R33", "U53", "R51"),
                    listOf("U98", "R91", "D20", "R16", "D67", "R40", "U7", "R15", "U6", "R7"),
                    410
                )
            ) { wire1, wire2, expectedAnswer ->
                part2(wire1, wire2) shouldBe expectedAnswer
            }
        }

        "Part 2 answer" {
            val (wire1, wire2) =
                getInputLines("day3.txt")
                    .map { it.split(",") }
                    .toList()

            val steps = part2(wire1, wire2)

            output(
                day = 3,
                part = 1,
                answer = steps
            )

            steps shouldBe 134662
        }
    }
}
package com.jamescurrent.adventofcode2019.day6

import com.jamescurrent.adventofcode2019.util.getInputLines
import com.jamescurrent.adventofcode2019.util.output
import io.kotlintest.provided.BaseStringSpec
import io.kotlintest.shouldBe

internal class Day6Test : BaseStringSpec() {
    init {
        "Part 1 given example" {
            val input =
                listOf(
                    "COM)B",
                    "B)C",
                    "C)D",
                    "D)E",
                    "E)F",
                    "B)G",
                    "G)H",
                    "D)I",
                    "E)J",
                    "J)K",
                    "K)L"
                )

            input.map { it.orbitSplit() }.makeNode().walkOrbits() shouldBe 42
        }

        "Part 1 answer" {
            val answer = getInputLines("day6.txt")
                .toList()
                .map { it.orbitSplit() }
                .makeNode()
                .walkOrbits()

            output(
                day = 6,
                part = 1,
                answer = answer
            )

            answer shouldBe 251208
        }

        "Part 2 given example" {
            listOf(
                "COM)B",
                "B)C",
                "C)D",
                "D)E",
                "E)F",
                "B)G",
                "G)H",
                "D)I",
                "E)J",
                "J)K",
                "K)L",
                "K)YOU",
                "I)SAN"
            ).findMinOrbitalTransfersToSanta() shouldBe 4
        }

        "Part 2 answer" {
            val answer = getInputLines("day6.txt")
                .toList()
                .findMinOrbitalTransfersToSanta()

            output(
                day = 6,
                part = 2,
                answer = answer
            )

            answer shouldBe 397
        }
    }
}
